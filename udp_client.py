import asyncio


class UdpClient(asyncio.DatagramProtocol):
    def __init__(self, message='hello'):
        self.message = message
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        print('connection made')
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print('data received {}'.format(data.decode()))

    def error_received(self, exc):
        print('exception happened {}'.format(exc))

    def connection_lost(self, exc):
        print('exception happened {}'.format(exc))
        loop = asyncio.get_event_loop()
        loop.stop()
