#!/usr/bin/env python
import asyncio
import click
from udp_client import UdpClient


@click.command()
@click.option(
    '--client_numbers',
    default=1,
    prompt='number of client',
    help='number of client to be generated')
def main(client_numbers):
    loop = asyncio.get_event_loop()
    client = loop.create_datagram_endpoint(lambda: UdpClient(), remote_addr=('127.0.0.1', 9999))
    transport, protocol = loop.run_until_complete(client)
    print(client_numbers)
    loop.run_forever()
    transport.close()
    loop.close()


if __name__ == '__main__':
    main()
